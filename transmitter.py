import socket
import pygame


IP = ""
port = 5005

# Buttons for piHut USB SNES controller
buttons = \
    {
        1: "BUTTON_A",
        2: "BUTTON_B",
        3: "BUTTON_Y",
        0: "BUTTON_X",
        5: "SHOULDER_RIGHT",
        4: "SHOULDER_LEFT",
        9: "BUTTON_START",
        8: "BUTTON_SELECT"
    }

# Buttons mapping for Xbox 360 controller
# buttons = \
#     {
#         1: "BUTTON_A",
#         0: "BUTTON_B",
#         3: "BUTTON_Y",
#         2: "BUTTON_X",
#         5: "SHOULDER_RIGHT",
#         4: "SHOULDER_LEFT",
#         7: "BUTTON_START",
#         6: "BUTTON_SELECT"
#     }

# Key mappings for keyboard
KEYS = \
    {
        'w': 'HAT_UP',
        'a': 'HAT_LEFT',
        's': 'HAT_DOWN',
        'd': 'HAT_RIGHT',
    
        'r': "BUTTON_A",
        't': "BUTTON_B",
        'f': "BUTTON_Y",
        'g': "BUTTON_X",
        'e': "SHOULDER_RIGHT",
        'q': "SHOULDER_LEFT",
        'o': "BUTTON_START",
        'p': "BUTTON_SELECT"
    }

upDownAxis = 1
leftRightAxis = 0

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send(msg):
    print(msg)
    msg = bytes(msg, encoding='utf-8')
    sock.sendto(msg, (IP, port))


def main():

    pygame.init()
    pygame.display.set_mode((200,200))
    print("Firing button presses at: {}:{}".format(IP, port))
    pygame.display.set_caption("{}:{}".format(IP,port))

    joystick_count = pygame.joystick.get_count()
    if joystick_count < 1:
        raise Exception("No gamepads found")

    joy = pygame.joystick.Joystick(0)
    joy.init()

    # Become true when the button on the axis is pressed
    # So we know which button has come back up when the axis position returns to 0
    upIsDown = False
    downIsDown = False
    leftIsDown = False
    rightIsDown = False

    while True:

        for event in pygame.event.get():

            if event.type == pygame.KEYUP:
                key_name = pygame.key.name(event.key)
                print("key up: " + key_name)
                send("{}_UP".format(KEYS[key_name]))
                
            if event.type == pygame.KEYDOWN:
                key_name = pygame.key.name(event.key)
                print("key down: " + pygame.key.name(event.key))
                send("{}_DOWN".format(KEYS[key_name]))
                
            if event.type == pygame.QUIT:
                pygame.quit()

            if event.type == pygame.JOYBUTTONDOWN:
                send("{}_DOWN".format(buttons[event.button]))


            if event.type == pygame.JOYBUTTONUP:
                send("{}_UP".format(buttons[event.button]))

            if event.type == pygame.JOYHATMOTION:
                x,y = event.value
                if y == 0:
                    if upIsDown:
                        upIsDown = False
                        send("HAT_UP_UP")
                    elif downIsDown:
                        downIsDown = False
                        send("HAT_DOWN_UP")
                elif y < 0:
                    send("HAT_DOWN_DOWN")
                    downIsDown = True
                elif y > 0:
                    send("HAT_UP_DOWN")
                    upIsDown = True

                if x == 0:
                    if rightIsDown:
                        rightIsDown = False
                        send("HAT_RIGHT_UP")
                    elif leftIsDown:
                        leftIsDown = False
                        send("HAT_LEFT_UP")
                elif x > 0:
                    send("HAT_RIGHT_DOWN")
                    rightIsDown = True
                elif x < 0:
                    send("HAT_LEFT_DOWN")
                    leftIsDown = True

            if event.type == pygame.JOYAXISMOTION:
                # If the event is on the up/down axis
                if event.axis == upDownAxis:
                    if event.value == 0:
                        if upIsDown:
                            upIsDown = False
                            send("HAT_UP_UP")
                        elif downIsDown:
                            downIsDown = False
                            send("HAT_DOWN_UP")
                    elif event.value > 0:
                        send("HAT_DOWN_DOWN")
                        downIsDown = True
                    elif event.value < 0:
                        send("HAT_UP_DOWN")
                        upIsDown = True

                # If it's left/right axis
                if event.axis == leftRightAxis:
                    if event.value == 0:
                        if leftIsDown:
                            leftIsDown = False
                            send("HAT_LEFT_UP")
                        elif rightIsDown:
                            rightIsDown = False
                            send("HAT_RIGHT_UP")
                    elif event.value > 0:
                        send("HAT_RIGHT_DOWN")
                        rightIsDown = True
                    elif event.value < 0:
                        send("HAT_LEFT_DOWN")
                        leftIsDown = True


if __name__ == "__main__":
    main()
