import socket
import serial
from time import sleep

sleep(2)

IP = "127.0.0.1"
port = 5005

arduinoSerialPort = "/dev/ttyUSB1"
baud = 115200

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((IP, port))


def convertToInt(arrayInput):
    return int(ord(arrayInput[:1]))


def receive():
    data, addr = sock.recvfrom(1024)
    return data.decode()


def main():

    print("Listening to button presses from: {}:{}".format(IP, port))
    ser = serial.Serial(arduinoSerialPort, baud)

    SNESData1 = 0x00
    SNESData2 = 0x00

    ser.write([0x10])

    while True:
        data = receive()
        print(data)

        if True:
            # Up
            if data == "HAT_UP_DOWN":
                SNESData2 = SNESData2 | 0b00010000
            elif data == "HAT_UP_UP":
                SNESData2 = SNESData2 & 0b11101111
            # Down
            elif data == "HAT_DOWN_DOWN":
                SNESData2 = SNESData2 | 0b01000000
            elif data == "HAT_DOWN_UP":
                SNESData2 = SNESData2 & 0b10111111
            # Left
            elif data == "HAT_LEFT_DOWN":
                SNESData2 = SNESData2 | 0b00100000
            elif data == "HAT_LEFT_UP": 
                SNESData2 = SNESData2 & 0b11011111
            # Right
            elif data == "HAT_RIGHT_DOWN":
                SNESData2 = SNESData2 | 0b10000000
            elif data == "HAT_RIGHT_UP": 
                SNESData2 = SNESData2 & 0b01111111
            # Y
            elif data == "BUTTON_Y_DOWN":
                SNESData2 = SNESData2 | 0b00000010
            elif data == "BUTTON_Y_UP":
                SNESData2 = SNESData2 & 0b11111101
            # B
            elif data == "BUTTON_B_DOWN":
                SNESData2 = SNESData2 | 0b00000001
            elif data == "BUTTON_B_UP": 
                SNESData2 = SNESData2 & 0b11111110
            # A
            elif data == "BUTTON_A_DOWN":
                SNESData1 = SNESData1 | 0b00000001
            elif data == "BUTTON_A_UP":
                SNESData1 = SNESData1 & 0b11111110
            # X
            elif data == "BUTTON_X_DOWN":
                SNESData1 = SNESData1 | 0b00000010
            elif data == "BUTTON_X_UP":
                SNESData1 = SNESData1 & 0b11111101
            # L
            elif data == "SHOULDER_LEFT_DOWN":
                SNESData1 = SNESData1 | 0b00000100
            elif data == "SHOULDER_LEFT_UP":
                SNESData1 = SNESData1 & 0b11111011
            # R
            elif data == "SHOULDER_RIGHT_DOWN":
                SNESData1 = SNESData1 | 0b00001000
            elif data == "SHOULDER_RIGHT_UP":
                SNESData1 = SNESData1 & 0b11110111
            # Start
            elif data == "BUTTON_START_DOWN":
                SNESData2 = SNESData2 | 0b00001000
            elif data == "BUTTON_START_UP":
                SNESData2 = SNESData2 & 0b11110111
            # Select
            elif data == "BUTTON_SELECT_DOWN":
                SNESData2 = SNESData2 | 0b00000100
            elif data == "BUTTON_SELECT_UP":
                SNESData2 = SNESData2 & 0b11111011

            ser.write([SNESData1])
            ser.write([SNESData2])
            print("%s %s" % (hex(SNESData1), hex(SNESData2)))
        

if __name__ == "__main__":
    main()